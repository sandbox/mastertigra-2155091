<?php

/**
 * mrtigra config form.
 */
function gnome_nations_config_form($form, &$form_state) {
  $form = array();
 
  $form['gnome_nations_rid'] = array(
    '#type' => 'textfield',
    '#title' => t(gnome_nations_lang("rid_header")),
    '#default_value' => variable_get('gnome_nations_rid', ""),
    '#description' => t(gnome_nations_lang("rid_descr")),
    '#required' => FALSE,
  );
  
  $form['gnome_nations_description'] = array(
    '#type' => 'item',
    '#title' => gnome_nations_lang("guide_header"),
    '#markup' => gnome_nations_lang("guide"),
  );
  return system_settings_form($form);
}